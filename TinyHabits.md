## Q1. In this video, what was the most interesting story or idea for you?

**Answer**

The equation B = MAT, along with its visual depiction, encapsulates the notion that while long-term motivation may wane, sustained behavioral change is achievable through the interplay of Motivation, Ability, and Triggers (MAT). By cultivating small habits and leveraging timely triggers, individuals can navigate beyond the fluctuations in motivation, ensuring lasting and meaningful changes in behavior.

## Q2. How can you use B = MAP to make making new habits easier? What are M, A and P.

**Answer**

M - Motivation. A - Ability. P - Prompt. While attaining significant habits may be challenging, we can cultivate their miniature counterparts on a daily basis. Utilizing the "After I <some_task>, I will <tiny_habit>" trigger allows for the accomplishment of various goals. Success and sustainability lie in embracing small steps.

## Q3. Why it is important to "Shine" or Celebrate after each successful completion of habit?

**Answer**
The core principle here is that it naturally boosts an individual's confidence and motivation, essentially translating to self-encouragement.

## Q4. In this video, what was the most interesting story or idea for you?

**Answer**
The four phases include Observing, Desiring, Acting, and Enjoying. During the execution phase, repetition is crucial, requiring us to initiate repeatedly and refine our starting point. It's essential to remember that the rewards of positive habits are deferred.

## Q5. What is the book's perspective about Identity?

**Answer**
It's essential to integrate a habit into our identity, making it a natural part of who we are. Setting specific goals can limit our overall sense of happiness.

## Q6. Write about the book's perspective on how to make a habit easier to do?

**Answer**
Cumulative small choices lead to significant outcomes. Instead of tackling substantial tasks, break them down into small habits and consistently accomplish them. Environmental design also contributes to the formation of habits.

## Q7. Write about the book's perspective on how to make a habit harder to do?

**Answer**
To make a habit harder to do, "Atomic Habits" suggests focusing on the following:

1. **Make it Invisible (Cue):** Reduce the visibility or noticeability of the habit's trigger.

2. **Make it Unattractive (Craving):** Diminish the appeal or desire for the habit by associating it with negative consequences.

3. **Make it Difficult (Response):** Increase the effort required to perform the habit by introducing obstacles or additional steps.

4. **Make it Unsatisfying (Reward):** Minimize the pleasure or rewards associated with the habit to discourage its repetition.


## Q8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

**Answer**
I want to make exercising a regular thing. I'll try doing it in the morning for about an hour every day. This way, I hope to easily remember to do it (making the cue obvious), find it enjoyable (making the habit attractive), and feel good about sticking to it (making the response satisfying).

## Q9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

**Answer**

I want to reduce the habit of saying yes to every work request. To achieve this, I can try the following:

1. **Make the Cue Invisible:** Turn off unnecessary work notifications.

2. **Make the Process Unattractive:** Remind myself of the stress caused by overcommitting.

3. **Make it Hard:** Set a rule to wait before responding to work requests.

4. **Make the Response Unsatisfying:** Reflect on past instances where saying "yes" led to burnout or compromised quality.
