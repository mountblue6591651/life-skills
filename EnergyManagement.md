## Q1. What are the activities you do that make you relax - Calm quadrant?

**Answer :**

1. **Napping:**
   - Regularly taking short naps provides a quick and effective way to recharge, enhancing mood and cognitive performance.

2. **Listening to Music:**
   - Enjoying a diverse range of music genres serves as a stress-reliever for me, creating a soothing atmosphere for relaxation.

3. **Deep Breathing:**
   - Whenever I feel panic or anxitey like situations I tend to take deep breaths to calm myself.

## Q2. When do you find getting into the Stress quadrant?

**Answer :**

1. **Before Reviews:**
   - When sir is calling other students one-by-one, but as soon as my turn comes I am all okay.

2. **Fight with known one's**

## Q3. How do you understand if you are in the Excitement quadrant?

**Answer :**
When I am in excitement quadrant I feel more focused and happy and not tired.


## Q4. Paraphrase the Sleep is your Superpower video in your own words in brief.

**Answer :**

- So, getting enough sleep is super important for your brain – it helps with remembering stuff and learning new things.
- Sleep does a big job in keeping your body healthy too, by messing with hormones that control hunger and metabolism.
- If you don't get enough sleep, you're more likely to end up with issues like being overweight, diabetes, heart problems, and even a higher chance of kicking the bucket.
- Most grown-ups should aim for 7-8 hours of sleep each night, but everyone's a bit different.
- Want better sleep? Stick to a regular bedtime, do something relaxing before you hit the hay, and cut back on the caffeine and booze – that should do the trick!

## Q5. What are some ideas that you can implement to sleep better?

**Answer :**

- Keep a steady sleep schedule, like hitting the hay and waking up at the same time every day.
- Make a chill bedtime routine – do something calming before you crash out.
- Skip the caffeine and booze before bedtime; they can mess with your sleep vibes.
- Set up a cozy sleep zone – think comfy pillows, cool sheets, and all that good stuff.
- Cut down on screen time before you hit the sack; that bright light messes with your sleep mojo.
- Get moving with some regular exercise – it helps with the sleep game.
- Chill out and manage stress by trying some relaxation tricks – find what works for you.

## Q6. Paraphrase the video - Brain Changing Benefits of Exercise.

**Answer :**

- Grow Your Brain with Exercise: Doing the exercise thing helps your brain's hippocampus grow – that's the memory and learning boss.
- Memory Boost: Move your body regularly, and you'll see upgrades in both short-term and long-term memory – no more brain farts.
- Get Your Brain on Point: Sweating it out in a workout amps up your focus, attention, and concentration – like a brain power-up.
- Stress? What Stress? Keeping up with regular exercise is like a superhero move against stress and anxiety – they don't stand a chance.
- Feel-Good Vibes and Brain Smarts: Working out isn't just for the body; it's a mood booster and a brain enhancer – making you more creative and a problem-solving ninja.


## Q7. What are some steps you can take to exercise more?

**Answer :**

- Keep it Real with Fitness Goals: Don't go too crazy, set goals that you can actually hit – no need for superhero stuff.
- Throw in Fun Workouts: Mix it up with stuff you enjoy, whether it's a walk, jog, swim, or dance party – gotta keep it interesting.
- Get a Workout Buddy: Grab a friend for workouts – it's more fun and keeps you from slacking.
- Take it Easy at First: Don't go all-out from the start; start slow and steady, then crank it up when you're ready.
- Plan Exercise Time: Schedule in some workout time regularly – make it a part of your routine that you can stick to.