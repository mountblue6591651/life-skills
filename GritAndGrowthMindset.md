### Question 1
**Answer**

Angela Duckworth emphasizes the importance of "grit," which is the combination of passion and perseverance for long-term goals, as a crucial factor in determining success. She discusses the need to better understand and foster grit, especially in education, and suggests exploring the concept of a "growth mindset." Duckworth encourages ongoing efforts to test and refine strategies for cultivating grit in children. 

### Question 2
**Answer**

The video talks about how people think about learning, using the idea of "growth mindset." It's split into two ways of thinking: fixed mindset and growth mindset. Fixed mindset believes that skills are natural and can't be changed, while growth mindset believes that skills can be developed. The video explains that having a growth mindset helps people learn better because they see effort, challenges, mistakes, and feedback as opportunities for improvement. It emphasizes that cultivating a growth mindset is important for better learning in different areas like sports, education, and work.

### Q3 What is the Internal Locus of Control? What is the key point in the video?
**Answer**

The internal locus of control means believing you have control over your life. In the video, a study showed that when kids were told they succeeded because of hard work, they were more motivated and enjoyed challenges. This is linked to having an internal locus of control, where you feel in control of your outcomes. The key point is that believing you can control and influence your life leads to better motivation and success. To adopt it, solve problems in your life and appreciate that your actions make a difference.

### Q4 What are the key points mentioned by speaker to build growth mindset.

**Answer**
1. Believe in your ability to figure things out.
2. Question your assumptions and challenge limiting beliefs.
3. Develop your own life curriculum for continuous learning.
4. Embrace and honor the struggle as part of the growth process.
5. Maintain resilience in the face of challenges and setbacks.

### Q5 What are your ideas to take action and build Growth Mindset?

**Answer**

1. **Take Charge of Learning:**
   - Be fully responsible for your own learning. Understand that your progress is in your hands.

2. **Stick with Problems:**
   - Don't give up on problems. Keep working on them until you find a solution. 

3. **Put in Effort for Understanding:**
   - Know that the more effort you put in, the better you'll understand. Learning requires continuous effort.

4. **Understand Code Completely:**
   - Don't write code you don't get. Make sure you understand every part of the code you write.

5. **See Challenges as Opportunities:**
   - Look at new things as chances to learn, not as things to stress about. Confusion and mistakes are ways to improve.


