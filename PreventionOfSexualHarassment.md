## Q1. What kinds of behaviour cause sexual harassment?

**Verbal Harassment:**
- *Comments about Clothing and Body:* Inappropriate remarks about what someone is wearing or comments about their physical appearance.
- *Sexual or Gender-based Jokes or Remarks:* Jokes or remarks that are sexually explicit or based on gender stereotypes.
- *Requesting Sexual Favors or Repeatedly Asking Someone Out:* Unwanted advances, requests for sexual activities, or persistent invitations for dates.
- *Sexual Innuendos:* Subtle and often suggestive remarks or allusions with a sexual connotation.
- *Threats:* Expressing intentions of harm or negative consequences related to sexual compliance.
- *Spreading Rumors about Personal or Sexual Life:* Spreading false or private information about someone's personal or sexual life.
- *Using Foul and Obscene Language:* Employing offensive or vulgar language with sexual undertones.

**Visual Harassment:**
- *Posters, Drawings, Pictures, Screen Savers, Cartoons:* Creating or displaying sexually explicit or suggestive visual content in the workplace.
- *Emails or Texts of a Sexual Nature:* Sending electronic messages with explicit or inappropriate sexual content.

**Physical Harassment:**
- *Sexual Assault:* Any unwanted physical contact or sexual activity without consent.
- *Impeding or Blocking Movement:* Physically preventing someone from moving freely.
- *Inappropriate Touching:* Non-consensual physical contact, including kissing, hugging, patting, stroking, or rubbing.
- *Sexual Gesturing, Leering, or Staring:* Using suggestive body language, making prolonged suggestive looks, or staring in a sexually suggestive manner.

These behaviors, when unwelcome, severe, or pervasive, can create a hostile work environment or be used in a quid pro quo scenario, both of which are forms of sexual harassment. It's crucial to recognize and address such behaviors to maintain a safe and respectful workplace.

## Q2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

If I were to face or witness any incident or repeated incidents of bullying or harassment in the workplace, I would take the following steps:

1. **Report the Incident:**
   - I would promptly follow the established procedures set by my employer for reporting incidents or complaints. This may involve reaching out to a designated person, such as a supervisor, manager, or HR representative.

2. **Document the Details:**
   - Keeping a detailed record of the incident is crucial. I would make notes on the date, time, location, individuals involved, and a comprehensive description of the behavior. Such documentation could prove valuable if further action is necessary.

3. **Follow Employer Policies:**
   - Familiarizing myself with my employer's policies and procedures regarding bullying and harassment is essential. I would ensure strict adherence to these guidelines when reporting incidents and follow any provided guidance.

4. **Seek Support:**
   - If I feel comfortable, I would discuss the incident with a trusted colleague, supervisor, or HR representative. Seeking support can offer guidance on the appropriate steps to take and contribute to my overall well-being.

5. **Participate in Training:**
   - Actively participating in any workplace training on preventing and addressing bullying and harassment would be a priority. Understanding the policies and procedures empowers me to navigate such situations effectively.

6. **Stay Informed:**
   - Staying informed about my rights as an employee and the resources available to address workplace issues is crucial. This knowledge empowers me to take appropriate action.

In summary, reporting incidents of bullying and harassment is not just a responsibility; it's a personal commitment to maintaining a healthy and safe work environment. If I encounter challenges or feel that the issue persists, seeking guidance from relevant authorities or resources provided by my employer, such as an online toolkit or the human resources department, would be my next step.
