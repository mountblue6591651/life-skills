## Which point(s) were new to you?

**Answer:**
- Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.

- Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

**Answer:**

I need to improve on my attention span and try to increase it.

To do it I can do the following tasks-

- Always keep my phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps.

- Block social media apps during work hours.

- Use tools to track time while using apps and improve productivity.

- Exercise daily.