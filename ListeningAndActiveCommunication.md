### Q1. What are the steps/strategies to do Active Listening?
**Answer:**

1. **Fully Hear and Comprehend:**
   - Focus on understanding the meaning of the speaker's words.

2. **Avoid Distractions:**
   - Stay mentally present, avoiding internal distractions.

3. **Resist Interrupting:**
   - Let the speaker finish before responding.

4. **Use Door Openers:**
   - Employ phrases showing interest to encourage the speaker.

5. **Show Interest with Body Language:**
   - Use non-verbal cues like nodding and eye contact.

6. **Take Notes and Paraphrase:**
   - Capture key points in notes and paraphrase to confirm understanding.
  
### Q2. According to Fisher's model, what are the key points of Reflective Listening?

**Answer:**

Dalmar Fisher, a professor at Boston College, developed a way to truly listen to others called Reflective Listening. It's like a toolkit with these key parts:

1. **Pay Full Attention:**
   - When you talk to someone, focus completely on them. Try to get rid of any distractions so you can really listen.

2. **Understand Their View:**
   - Imagine things from their perspective, even if you don't agree. Be open-minded and caring, so they feel comfortable sharing.

3. **Feel the Emotion:**
   - Pick up on their mood not just from words but how they say things. Listen to their tone, body language, and expressions. Match their feelings.

4. **Say it in Your Words:**
   - After they speak, tell them what you understood using your own words. It's different from just repeating; you're sharing what you got from their message.


### Q3. What are the obstacles in your listening process?

**Answer:**

1. **Distractions:**
   - External factors such as noise, interruptions, or environmental disturbances can hinder effective listening.

2. **Preoccupation:**
   - Personal thoughts, concerns, or preoccupations may divert attention away from the speaker's message.

3. **Impatience:**
   - Eagerness to respond or a lack of patience can lead to premature interruptions, preventing the speaker from fully expressing themselves.

4. **Lack of Clarification:**
   - Failure to seek clarification when faced with ambiguous or unclear information can contribute to misunderstandings.


### Q4. What can you do to improve your listening?

**Anserr:**

1. **Mitigating Distractions:**
   - Choose a quiet and focused environment for conversations.
   - Minimize potential interruptions, such as silencing electronic devices.
   - Practice active listening techniques to stay fully engaged with the speaker.

2. **Addressing Mind Wandering:**
   - Develop mindfulness techniques to stay present during discussions.
   - Set aside personal concerns before entering conversations.
   - Practice mental exercises to improve concentration and attention.

3. **Cultivating Patience:**
   - Remind yourself to listen attentively without rushing to respond.
   - Allow natural pauses in the conversation for the speaker to express themselves fully.
   - Practice active listening as a form of patience, letting the speaker have their say.

4. **Clarifying Information:**
   - Encourage a culture of open communication, where asking for clarification is welcomed.
   - Summarize key points during the conversation to ensure mutual understanding.
   - Be proactive in seeking additional information when faced with ambiguity.


### Q5. When do you switch to Passive communication style in your day to day life?

**Answer:**

- **Maintaining Harmony:**
   - When the primary goal is to avoid conflict or maintain a peaceful atmosphere, I opt for a passive communication style to prevent potential disagreements.

- **Lack of Confidence:**
   - When I lack confidence or fear negative reactions I choose a passive style to avoid drawing attention to myslef or my opinions.

- **Avoiding Criticism:**
   - When I am concerned about criticism or rejection, I use a passive style to minimize the chance of receiving negative feedback.

- **Preventing Disruption:**
   - In group settings or collaborative environments, I adopt a passive style to prevent disruptions or maintain a smooth flow of interactions.

- **Respecting Authority:**
   - When interacting with figures of authority, I prefer a passive communication style out of respect or deference.


### Q6. When do you switch into Aggressive communication styles in your day to day life?

**Answer:**

- **Feeling Really Angry:**
   - When I am super mad, I use aggressive communication to show just how upset I are.

- **Feeling Threatened:**
   - If I feel like I am in danger, I act aggressively to protect myself.

- **Wanting to Be in Charge:**
   - I sometimes use aggression to try and control a situation or make sure others listen to me.

- **Protecting Personal Space:**
   - When I feel like my personal space is invaded, I react aggressively to set boundaries.

### Q7. When do you switch into Passive Aggressive communication styles in your day to day life?

**Answer:**

1. **Frustration or dissatisfaction:**
   When I feel frustrated or dissatisfied but am hesitant to express my feelings directly, I use sarcasm or taunts as a way to subtly convey my discontent.

2. **Expressing passive resistance:**
   When I want to resist complying with a request or suggestion but don't want to overtly refuse, I use sarcasm or taunts to express my resistance indirectly.

3. **Seeking attention or validation:**
   Sarcasm, especially when delivered in a witty or clever manner, can sometimes be a way for me to seek attention, approval, or validation from others.

### Q8. How can you make your communication assertive? 

**Answer:**

**Recognize and name what you need:**
   - Learn to recognize and name your needs. Dig deeper to understand the underlying needs behind your requests. For instance, if you need a day off work, consider whether it's due to burnout and a desire for a more sustainable work schedule.

**Start with low-stakes situations:**
   - Practice assertive communication in low-stakes situations to build confidence. Begin with safe people and easy scenarios before tackling more challenging or high-stakes conversations, such as those with authority figures.

**Be aware of your body:**
   - Pay attention to your body language and tone of voice. Ensure that your non-verbal cues align with your assertive communication. Being aware of your body can help you regulate emotions and prevent extreme reactions in challenging situations.

**Don't wait:**
   - Address problematic situations early on. Speak up about issues that bother you as soon as possible to prevent them from becoming recurring problems. Waiting to address mistreatment or unmet needs may lead to the behavior continuing or worsening.
